let startMsgFunc = {
    "type": "list",
    "msgid": "msgId",
    "title": "Hello!",
    "body": "Welcome to Haier India. How may I help you?",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select Option"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Buy a Home Appliance",
                    "description": "",
                    "type": "text",
                    "postbackText": "Buy a Home Appliance"
                },
                {
                    "title": "Need a Demo",
                    "description": "",
                    "type": "text",
                    "postbackText": "Need a Demo"
                },
                {
                    "title": "Customer Care",
                    "description": "",
                    "type": "text",
                    "postbackText": "Customer Care"
                },
                {
                    "title": "Order Status",
                    "description": "",
                    "type": "text",
                    "postbackText": "Order Status"
                },
                {
                    "title": "Know about Haier India",
                    "description": "",
                    "type": "text",
                    "postbackText": "Know about Haier India"
                }
            ]
        }
    ]
}

let appliancesfunc = {
    "type": "list",
    "msgid": "msgId",
    "title": "",
    "body": "Please choose the Product.",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select Option"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Refrigerators",
                    "description": "",
                    "type": "text",
                    "postbackText": "Refrigerators"
                },
                {
                    "title": "Washing Machines",
                    "description": "",
                    "type": "text",
                    "postbackText": "Washing Machines"
                },
                {
                    "title": "Air Solutions",
                    "description": "",
                    "type": "text",
                    "postbackText": "Air Solutions"
                },
                {
                    "title": "Televisions",
                    "description": "",
                    "type": "text",
                    "postbackText": "Televisions"
                },
                {
                    "title": "Deep Freezer",
                    "description": "",
                    "type": "text",
                    "postbackText": "Deep Freezer"
                },
                {
                    "title": "Water Heaters",
                    "description": "",
                    "type": "text",
                    "postbackText": "Water Heaters"
                },
                {
                    "title": "Kitchen Appliances",
                    "description": "",
                    "type": "text",
                    "postbackText": "Kitchen Appliances"
                },
                {
                    "title": "Special Offers",
                    "description": "",
                    "type": "text",
                    "postbackText": "Special Offers"
                }
            ]
        }
    ]
}

let thankyouselectionFunc = {

    "type": "quick_reply",
    "name": "quickreply",
    "alias": "",
    "msgid": "newUser",
    "content": {
        "type": "text",
        "text": 'Visit Our Website \r\nhttps://shop.haierindia.com/ \r\n \r\nThank you. We will get back to you.'
    },
    "options": [
        {
            "type": "text",
            "title": "Back to Home",
            "iconurl": "",
            "id": "",
            "isDuplicate": false,
            "name": "user"
        }
    ]
}

let Refrigeratorsfunc = {

    "type": "list",
    "msgid": "msgId",
    "title": "",
    "body": "Which type of Refrigerator are you looking for?",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select any"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Smart Refrigerators",
                    "description": "Refrigerators",
                    "type": "text",
                    "postbackText": "Smart Refrigerators"
                },
                {
                    "title": "Side by Side Ref",
                    "description": "Refrigerators",
                    "type": "text",
                    "postbackText": "Side by Side Ref"
                },
                {
                    "title": "Bottom Mounted Ref",
                    "description": "Refrigerators",
                    "type": "text",
                    "postbackText": "Bottom Mounted Ref"
                },
                {
                    "title": "Top Mounted Ref",
                    "description": "Refrigerators",
                    "type": "text",
                    "postbackText": "Top Mounted Ref"
                },
                {
                    "title": "Direct Cool Ref",
                    "description": "Refrigerators",
                    "type": "text",
                    "postbackText": "Direct Cool Ref"
                },
            ]
        }
    ]
}

let WashingMacginesfunc = {
    "type": "list",
    "msgid": "msgId",
    "title": "",
    "body": "Which type of Washing Machine are you looking for?",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select any"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Smart Washing Machines",
                    "description": "Washing Machines",
                    "type": "text",
                    "postbackText": "Smart Washing Machines"
                },
                {
                    "title": "Front Load",
                    "description": "Washing Machines",
                    "type": "text",
                    "postbackText": "Front Load"
                },
                {
                    "title": "Top Load",
                    "description": "Washing Machines",
                    "type": "text",
                    "postbackText": "Top Load"
                },
            ]
        }
    ]
}

let AirSolutionsfunc = {
    "type": "list",
    "msgid": "msgId",
    "title": "",
    "body": "Which type of Air Solution are you looking for?",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select any"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Smart Air",
                    "description": "Conditioners",
                    "type": "text",
                    "postbackText": "Smart Air"
                },
                {
                    "title": "Tower",
                    "description": "Air Conditioners",
                    "type": "text",
                    "postbackText": "Tower"
                },
                {
                    "title": "Inverter Split AC",
                    "description": "Air Conditioners",
                    "type": "text",
                    "postbackText": "Inverter Split AC"
                },
                {
                    "title": "Fixed Speed Split AC",
                    "description": "Air Conditioners",
                    "type": "text",
                    "postbackText": "Fixed Speed Split AC"
                },
                {
                    "title": "Air Purifiers",
                    "description": "",
                    "type": "text",
                    "postbackText": "Air Purifiers"
                },
            ]
        }
    ]
}

let Televisionsfunc = {
    "type": "list",
    "msgid": "msgId",
    "title": "",
    "body": "Which type of Television are you looking for?",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select any"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Smart Television",
                    "description": "Television",
                    "type": "text",
                    "postbackText": "Smart Television"
                },
                {
                    "title": "LED",
                    "description": "Television",
                    "type": "text",
                    "postbackText": "LED"
                },
            ]
        }
    ]
}

let WaterHeatersfunc = {

    "type": "list",
    "msgid": "msgId",
    "title": "",
    "body": "Which type of Water Heater are you looking for?",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select any"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Smart Water",
                    "description": "Heaters",
                    "type": "text",
                    "postbackText": "Smart Water"
                },
                {
                    "title": "Storage",
                    "description": "Water Heaters",
                    "type": "text",
                    "postbackText": "Storage "
                },
                {
                    "title": "Instant",
                    "description": "Water Heaters",
                    "type": "text",
                    "postbackText": "Instant"
                },
            ]
        }
    ]
}

let KitchenAppliancesfunc = {

    "type": "list",
    "msgid": "msgId",
    "title": "",
    "body": "Which type of Kitchen Appliances are you looking for?",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select any"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Smart Kitchen",
                    "description": "Appliances",
                    "type": "text",
                    "postbackText": "Smart Kitchen"
                },
                {
                    "title": "Built-in Appliances",
                    "description": "Kitchen Appliances",
                    "type": "text",
                    "postbackText": "Built-in Appliances"
                },
                {
                    "title": "Convection MWO",
                    "description": "Kitchen Appliances",
                    "type": "text",
                    "postbackText": "Convection MWO"
                },
                {
                    "title": "T-Shaped Hood",
                    "description": "Kitchen Appliances",
                    "type": "text",
                    "postbackText": "T-Shaped Hood"
                },
            ]
        }
    ]
}

let NeedanDemofunc = {
    "type": "list",
    "msgid": "msgId",
    "title": "",
    "body": "Please select option",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select any"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Talk",
                    "description": "to a Product Expert",
                    "type": "text",
                    "postbackText": "Talk to a Product Expert"
                },
                {
                    "title": "Live Product Demo",
                    "description": "",
                    "type": "text",
                    "postbackText": "Live Product Demo"
                },
                {
                    "title": "Take a Virtual Tour",
                    "description": "",
                    "type": "text",
                    "postbackText": "Take a Virtual Tour"
                },
            ]
        }
    ]
}

let OrderStatusfunc = {

    "type": "list",
    "msgid": "msgId",
    "title": "",
    "body": "Please select option",
    "globalButtons": [
        {
            "type": "text",
            "title": "Please Select any"
        }
    ],
    "items": [
        {
            "title": "Options",
            "options": [
                {
                    "title": "Check my order status",
                    "description": "",
                    "type": "text",
                    "postbackText": "Check my order status"
                },
                {
                    "title": "Download my Invoice",
                    "description": "",
                    "type": "text",
                    "postbackText": "Download my Invoice"
                },
                {
                    "title": "Speak to an Executive",
                    "description": "",
                    "type": "text",
                    "postbackText": "Speak to an Executive"
                },
            ]
        }
    ]
}

let endfunc = {

    "type": "quick_reply",
    "name": "quickreply",
    "alias": "",
    "msgid": "newUser",
    "content": {
        "type": "text",
        "text": 'Thank you..'
    },
    "options": [
        {
            "type": "text",
            "title": "Back to Home",
            "iconurl": "",
            "id": "",
            "isDuplicate": false,
            "name": "user"
        }
    ]
}




module.exports = {
    startMsgFunc,
    appliancesfunc,
    thankyouselectionFunc,
    Refrigeratorsfunc,
    WashingMacginesfunc,
    AirSolutionsfunc,
    Televisionsfunc,
    WaterHeatersfunc,
    KitchenAppliancesfunc,
    NeedanDemofunc,
    OrderStatusfunc,
    endfunc
};