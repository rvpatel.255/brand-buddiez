const botScriptExecutor = require('bot-script').executor;
const scr_config = require('./scr_config.json');
const https = require('https');
let { startMsgFunc, appliancesfunc, thankyouselectionFunc, Refrigeratorsfunc, WashingMacginesfunc,
    AirSolutionsfunc, Televisionsfunc, WaterHeatersfunc, KitchenAppliancesfunc, NeedanDemofunc,
    OrderStatusfunc, endfunc } = require('./func.js');

let questionArray = [
    "Please enter your full name",
    "Please enter your contact number",
    "Please enter your E-mail ID",
    "Please enter your Pincode",
];

function MessageHandler(context, event) {
    let msg = event.message;
    let repMsg = "";
    var usermsg = event.messageobj.text.toLowerCase();

    console.log("Incoming ==============> " + event.messageobj);
    if (usermsg === "hi" || usermsg === "hello" || usermsg === "back to home" || usermsg === "home" || usermsg === "start") {
        console.log("start ==============> " + event.messageobj);
        repMsg = startMsgFunc;
    } else if (msg.startsWith("Buy a Home Appliance")) {
        console.log("Buy a Home Appliance Options ==============> " + event.messageobj);
        repMsg = appliancesfunc;
    } else if (msg.startsWith("Refrigerators")) {
        console.log("Refrigerator Options ==============> " + event.messageobj);
        repMsg = Refrigeratorsfunc;
    } else if (msg.startsWith("Washing Machines")) {
        console.log("Washing Machines Options ==============> " + event.messageobj);
        repMsg = WashingMacginesfunc;
    } else if (msg.startsWith("Air Solutions")) {
        console.log("Air Solution Options =============>" + event.messageobj);
        repMsg = AirSolutionsfunc;
    } else if (msg.startsWith("Televisions")) {
        console.log(" Televisions Options =============>" + event.messageobj);
        repMsg = Televisionsfunc;
    } else if (msg.startsWith("Water Heaters")) {
        console.log(" Water Heaters Options =============>" + event.messageobj);
        repMsg = WaterHeatersfunc;
    } else if (msg.startsWith("Kitchen Appliances")) {
        console.log(" Kitchen Appliances Options =============>" + event.messageobj);
        repMsg = KitchenAppliancesfunc;
    } else if (msg.startsWith("Need a Demo")) {
        console.log(" Need a demo Options =============>" + event.messageobj);
        repMsg = NeedanDemofunc;
    } else if (msg.startsWith("Talk")) {
        repMsg = [
            "During Working hrs\r\n(10:30 am to 5:30 pm)\r\nConnect with our Product Expert on\r\n+919930055400",
            "During Non Working hrs\r\nPlease share your details with us and our Product Expert\r\nwill get in touch with you shortly.",
            questionArray[0]
        ];
        let saveObj = {
            "nameFlag": true
        }
        //repMsg = [questionArray[0]];
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
    } else if (context.simpledb.roomleveldata.nameFlag) {
        let saveObj = {
            "nameFlag": false,
            "name": msg,
            "contactFlag": true
        }
        repMsg = [questionArray[1]];
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
        context.sendResponse(JSON.stringify(repMsg));
    }
    else if (context.simpledb.roomleveldata.contactFlag) {
        let saveObj = {
            "contactFlag": false,
            "contact": msg,
            "EmailFlag": true
        }
        repMsg = [questionArray[2]];
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
        context.sendResponse(JSON.stringify(repMsg));
    }
    else if (context.simpledb.roomleveldata.EmailFlag) {
        let saveObj = {
            "EmailFlag": false,
            "Email": msg,
            "PincodeFlag": true
        }
        repMsg = [questionArray[3]];
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
        context.sendResponse(JSON.stringify(repMsg));
    }

    else if (context.simpledb.roomleveldata.PincodeFlag) {
        let saveObj = {
            "PincodeFlag": false,
            "Pincode": msg,
            "ThankyouFlag": true
        };
        console.log("pincode===============");
        console.log(context.simpledb.roomleveldata.PincodeFlag);
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
        repMsg = thankyouselectionFunc;
        context.sendResponse(JSON.stringify(repMsg));
    }

    else if (msg.startsWith("Live Product Demo") || msg.startsWith("Take a Virtual Tour")) {
        console.log("Need a demo Options =============>" + event.messageobj);
        repMsg = {
            "type": "quick_reply",
            "name": "quickreply",
            "alias": "",
            "msgid": "newUser",
            "content": {
                "type": "text",
                "text": "click on link \r\n \r\n http://haierapps.haierindia.com/livedemo/?spm=in.29009_pc.header_92843_20190530.4# \r\n \r\nThank you.."
            },
            "options": [
                {
                    "type": "text",
                    "title": "Back to Home",
                    "iconurl": "",
                    "id": "",
                    "isDuplicate": false,
                    "name": "user"
                }
            ]
        }


    } else if (msg.startsWith("Customer Care")) {
        console.log(" Customer Care Options =============>" + event.messageobj);
        repMsg = {
            "type": "quick_reply",
            "name": "quickreply",
            "alias": "",
            "msgid": "newUser",
            "content": {
                "type": "text",
                "text": "Need Clarity"
            },
            "options": [
                {
                    "type": "text",
                    "title": "Back to Home",
                    "iconurl": "",
                    "id": "",
                    "isDuplicate": false,
                    "name": "user"
                }
            ]
        }

    } else if (msg.startsWith("Order Status")) {
        //console.log(" Order Options =============>" + event.messageobj);
        repMsg = OrderStatusfunc;
    } else if (msg.startsWith("Check my order status") || msg.startsWith("Download my Invoice")) {
        console.log(" Order status Options =============>" + event.messageobj);
        repMsg = [
            "Please provide us with your Order Number (AlphaNumeric)"
        ]
    } else if (msg.startsWith("Speak to an Executive")) {
        console.log(" Order status Options =============>" + event.messageobj);
        repMsg = [
            "Please provide us with your Order Number (AlphaNumeric)",
            "Connect with our Team on \r\n +91 9930055800"
        ]
    } else if (msg.startsWith("Know about Haier India")) {
        console.log("Know about Haier India Options =============>" + event.messageobj);
        
        repMsg = {
            "type": "quick_reply",
            "name": "quickreply",
            "alias": "",
            "msgid": "newUser",
            "content": {
                "type": "text",
                "text": "Want to know about Haier India Click on below link \r\n \r\n https://shop.haierindia.com/about_us.php \r\n \r\n Thank you.."
            },
            "options": [
                {
                    "type": "text",
                    "title": "Back to Home",
                    "iconurl": "",
                    "id": "",
                    "isDuplicate": false,
                    "name": "user"
                }
            ]
        }
        
    } else if (
        //Refrigerators
        msg.startsWith("Smart Refrigerators") || msg.startsWith("Side by Side Ref") || msg.startsWith("Bottom Mounted Ref") || msg.startsWith("Top Mounted Ref") || msg.startsWith("Direct Cool Ref") ||
        //Washing machine
        msg.startsWith("Smart Washing Machines") || msg.startsWith("Front Load") || msg.startsWith("Top Load") ||
        //Air Solutions
        msg.startsWith("Smart Air") || msg.startsWith("Tower") || msg.startsWith("Inverter Split AC") || msg.startsWith("Fixed Speed Split AC") || msg.startsWith("Air Purifiers") ||
        //Televisions
        msg.startsWith("Smart Television") || msg.startsWith("LED") ||
        //Deep Freezer
        msg.startsWith("Deep Freezer") ||
        //Water Heater
        msg.startsWith("Smart Water") || msg.startsWith("Storage") || msg.startsWith("Instant") ||
        //Kitchen Appliances
        msg.startsWith("Smart Kitchen") || msg.startsWith("Built-in Appliances") || msg.startsWith("Convection MWO") || msg.startsWith("T-Shaped Hood") ||
        //Special Offers
        msg.startsWith("Special Offers")) {
        //console.log("All inner Options of Home appliances =============>" + event.messageobj);
        repMsg = [
            "In Order for us to help you better, please share your details  (*short privacy / usage tickable option)",
            questionArray[0]
        ];
        let saveObj = {
            "nameFlag": true
        }
        //repMsg = [questionArray[0]];
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
    } else if (context.simpledb.roomleveldata.nameFlag) {
        let saveObj = {
            "nameFlag": false,
            "name": msg,
            "contactFlag": true
        }
        repMsg = [questionArray[1]];
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
        context.sendResponse(JSON.stringify(repMsg));
    }
    else if (context.simpledb.roomleveldata.contactFlag) {
        let saveObj = {
            "contactFlag": false,
            "contact": msg,
            "EmailFlag": true
        }
        repMsg = [questionArray[2]];
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
        context.sendResponse(JSON.stringify(repMsg));
    }
    else if (context.simpledb.roomleveldata.EmailFlag) {
        let saveObj = {
            "EmailFlag": false,
            "Email": msg,
            "PincodeFlag": true
        }
        repMsg = [questionArray[3]];
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
        context.sendResponse(JSON.stringify(repMsg));
    }

    else if (context.simpledb.roomleveldata.PincodeFlag) {
        let saveObj = {
            "PincodeFlag": false,
            "Pincode": msg,
            "ThankyouFlag": true
        };
        console.log("pincode===============");
        console.log(context.simpledb.roomleveldata.PincodeFlag);
        context.simpledb.roomleveldata = Object.assign(context.simpledb.roomleveldata, saveObj);
        repMsg = thankyouselectionFunc;
        context.sendResponse(JSON.stringify(repMsg));


    } else if (msg.startsWith("Back to Home")) {
        //console.log("Back To Home Options =============>" + event.messageobj);
        repMsg = startMsgFunc;
    } else {
        console.log("Back To Home Options =============>" + event.messageobj);
        repMsg = endfunc;
    }
    context.sendResponse(JSON.stringify(repMsg));
    //context.sendResponse(msg);
}

function EventHandler(context, event) {
    context.simpledb.roomleveldata = {};
    MessageHandler(context, event);
}

function ScriptHandler(context, event) {
    debugger;
    let options = Object.assign({}, scr_config);
    options.current_dir = __dirname;
    options.default_message = "Sorry I am young and still learning. I am unable to understand your query.";
    // You can add any start point by just mentioning the <script_file_name>.<section_name>
    // options.start_section = "default.main";
    options.success = function (opm) {
        context.sendResponse(JSON.stringify(opm));
    };
    options.error = function (err) {
        context.console.log(err.stack);
        context.sendResponse("Sorry Some error occurred.");
    };
    botScriptExecutor.execute(options, event, context);
}

function HttpResponseHandler(context, event) {
    if (event.geturl === "http://ip-api.com/json")
        context.sendResponse('This is response from http \n' + JSON.stringify(event.getresp, null, '\t'));
}

function DbGetHandler(context, event) {
    context.sendResponse("testdbput keyword was last sent by:" + JSON.stringify(event.dbval));
}

function DbPutHandler(context, event) {

    context.sendResponse("testdbput keyword was last sent by:" + JSON.stringify(event.dbval));
}

function HttpEndpointHandler(context, event) {
    context.sendResponse('This is response from http \n' + JSON.stringify(event, null, '\t'));
}

function LocationHandler(context, event) {
    context.sendResponse("got location");
}

exports.onMessage = MessageHandler;
exports.onEvent = EventHandler;
exports.onHttpResponse = HttpResponseHandler;
exports.onDbGet = DbGetHandler;
exports.onDbPut = DbPutHandler;
if (typeof LocationHandler == 'function') {
    exports.onLocation = LocationHandler;
}
if (typeof HttpEndpointHandler == 'function') {
    exports.onHttpEndpoint = HttpEndpointHandler;
}